//
//  IssueTableViewCell.m
//  RPMagazine
//
//  Created by purong on 15/9/15.
//  Copyright © 2015 purong. All rights reserved.
//

#import "IssueTableViewCell.h"

@implementation IssueTableViewCell

- (void)awakeFromNib {
    [_issueCoverImageView setContentMode:UIViewContentModeScaleAspectFill];
    [self setupPreviewButton];
    [self setupPriceButton];
    [self setupLabels];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)setupPreviewButton{
    [_issuePreviewBtn setBackgroundColor:[UIColor whiteColor]];
    [_issuePreviewBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[_issuePreviewBtn layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [[_issuePreviewBtn layer]setBorderWidth:0.5];
    [[_issuePreviewBtn layer]setCornerRadius:5.0];
}

-(void)setupPriceButton{
    [_issuePriceBtn setBackgroundColor:[UIColor grayColor]];
    [_issuePriceBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[_issuePriceBtn layer]setBorderColor:[UIColor grayColor].CGColor];
    [[_issuePriceBtn layer]setBorderWidth:0.5];
    [[_issuePriceBtn layer]setCornerRadius:5.0];
}

-(void)setupLabels{
    [_issueNumberLabel setNumberOfLines:0];
    [_issueNumberLabel setLineBreakMode:NSLineBreakByWordWrapping];
    
    [_issueDescriptionLabel setNumberOfLines:0];
    [_issueDescriptionLabel setLineBreakMode:NSLineBreakByWordWrapping];
}

@end
