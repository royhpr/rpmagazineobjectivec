//
//  Constant.h
//  RPMagazine
//
//  Created by purong on 15/9/15.
//  Copyright © 2015 purong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constant : NSObject

//Images
#define IMG_RP_TITLE @"RobertParker_Title"
#define IMG_SEARCH @"search"

//Misc
#define BAR_COLOR_R 36.0
#define BAR_COLOR_G 17.0
#define BAR_COLOR_B 24.0
#define CUSTOM_COLOR_BASE 255.0
#define NAV_BAR_ICON_SIZE 30.0


@end
