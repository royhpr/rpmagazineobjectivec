//
//  AppDelegate.h
//  RPMagazine
//
//  Created by purong on 15/9/15.
//  Copyright © 2015 purong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

