//
//  IssueTableViewCell.h
//  RPMagazine
//
//  Created by purong on 15/9/15.
//  Copyright © 2015 purong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IssueTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *issueCoverImageView;
@property (weak, nonatomic) IBOutlet UILabel *issueNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *issueDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *issueDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *issuePreviewBtn;
@property (weak, nonatomic) IBOutlet UIButton *issuePriceBtn;

@end
