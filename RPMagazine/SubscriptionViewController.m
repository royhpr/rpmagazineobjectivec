//
//  SubscriptionViewController.m
//  RPMagazine
//
//  Created by purong on 23/9/15.
//  Copyright © 2015 purong. All rights reserved.
//

#import "SubscriptionViewController.h"

@interface SubscriptionViewController ()

@property(nonatomic, strong)NSMutableArray *subscriptionList;
@property(nonatomic, strong)NSMutableArray *searchList;

@property(nonatomic, strong)UISearchBar *subscriptionSearchBar;

@end

@implementation SubscriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Setup nav bar
    [self setupNavBar];
    
    //Setup list
    [self setupList];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [_subscriptionMainTable reloadData];
}

-(void)setupList{
    _subscriptionList = [[NSMutableArray alloc]init];
    _searchList = [[NSMutableArray alloc]init];
    
    //TODO: remove it, for testing purpose, real time data will be fetched from server
    IssueModel *testIssue = [[IssueModel alloc]init];
    [testIssue setIssueTitle:@"ISSUE 01"];
    [testIssue setIssuePrice:@"Free"];
    [testIssue setIssueDesc:@"This is a test issue, blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah"];
    [testIssue setIssueDate:@"June 2015"];
    [testIssue setIssueCoverImageURL:@"NewsstandIcon@2x.png"];
    
    [_subscriptionList addObject:testIssue];
}

-(void)setupNavBar{
    //add title image
    [self addNavTitleView];
    
    //Nav bar background color and tint color
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:BAR_COLOR_R/CUSTOM_COLOR_BASE green:BAR_COLOR_G/CUSTOM_COLOR_BASE blue:BAR_COLOR_B/CUSTOM_COLOR_BASE alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    //Add search button
    [self addSearchViewToNav];
}

-(void)addNavTitleView{
    UIImage *image = [UIImage imageNamed:IMG_RP_TITLE];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
}

-(void)addSearchViewToNav{
    UIImageView *searchImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, NAV_BAR_ICON_SIZE, NAV_BAR_ICON_SIZE)];
    [searchImageView setImage:[UIImage imageNamed:IMG_SEARCH]];
    UITapGestureRecognizer *searchSingleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapToSearchSubscriptions)];
    [searchSingleTap setNumberOfTapsRequired:1];
    [searchSingleTap setNumberOfTouchesRequired:1];
    [searchImageView setUserInteractionEnabled:YES];
    [searchImageView addGestureRecognizer:searchSingleTap];
    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc]initWithCustomView:searchImageView];
    self.navigationItem.rightBarButtonItem = searchButton;
}

-(void)tapToSearchSubscriptions{
    //Remove search view
    self.navigationItem.rightBarButtonItem = nil;
    
    //Add search bar instead
    _subscriptionSearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0.0, 0.0, self.navigationController.navigationBar.frame.size.width, NAV_BAR_ICON_SIZE)];
    [_subscriptionSearchBar setPlaceholder:@"Type to search issues..."];
    [_subscriptionSearchBar setTintColor:[UIColor colorWithRed:168.0/CUSTOM_COLOR_BASE green:115.0/CUSTOM_COLOR_BASE blue:104.0/CUSTOM_COLOR_BASE alpha:1.0]];
    _subscriptionSearchBar.delegate = self;
    
    [self.navigationItem setTitleView:_subscriptionSearchBar];
    [_subscriptionSearchBar becomeFirstResponder];
}

#pragma mark - Search bar delegate methods
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    searchBar.showsCancelButton = YES;
    
    [_searchList addObjectsFromArray:_subscriptionList];
    [_subscriptionList removeAllObjects];
    
    [_subscriptionMainTable reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self filterContentForSearchText:searchText];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    //Called when resign first respondant
    searchBar.showsCancelButton = NO;
    searchBar.text = @"";
    
    //Put back previous search list if it is not empty
    if(_searchList.count != 0){
        [_subscriptionList removeAllObjects];
        [_subscriptionList addObjectsFromArray:_searchList];
    }
    
    //Clear search list
    [_searchList removeAllObjects];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    
    //Remove search bar
    [_subscriptionSearchBar removeFromSuperview];
    _subscriptionSearchBar = nil;
    [self addNavTitleView];
    [self addSearchViewToNav];
    
    //Reload the isuue list
    [_subscriptionMainTable reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    //Not in use
}

- (void)filterContentForSearchText:(NSString*)searchText{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF.issueTitle contains[cd] %@ OR SELF.issueDate contains[cd] %@ OR SELF.issueDesc contains[cd] %@ OR SELF.issuePrice contains[cd] %@",
                                    searchText, searchText, searchText, searchText];

    [_subscriptionList removeAllObjects];
    [_subscriptionList addObjectsFromArray: [_searchList filteredArrayUsingPredicate:resultPredicate]];
    
    [_subscriptionMainTable reloadData];
}


#pragma mark - table view delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _subscriptionList.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"SubscriptionCell";
    
    SubscriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SubscriptionTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    IssueModel *currentIssue = (IssueModel*)[_subscriptionList objectAtIndex:[indexPath row]];
    cell.issueNumberLabel.text = currentIssue.issueTitle;
    cell.issueDateLabel.text = currentIssue.issueDate;
    cell.issueDescriptionLabel.text = currentIssue.issueDesc;
    [cell.issueCoverImageView setImage:[UIImage imageNamed:currentIssue.issueCoverImageURL]]; //This will require asyn load of image URL
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //Do any thing necessary after this issue is tapped
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 180.0;
}


#pragma mark - for interface rotation
- (BOOL)shouldAutorotate{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
