//
//  SubscriptionTableViewCell.h
//  RPMagazine
//
//  Created by purong on 23/9/15.
//  Copyright © 2015 purong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubscriptionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *issueCoverImageView;
@property (weak, nonatomic) IBOutlet UILabel *issueNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *issueDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *issueDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *issueReadBtn;

@end
