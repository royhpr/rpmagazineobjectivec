//
//  IssueViewController.h
//  RPMagazine
//
//  Created by purong on 15/9/15.
//  Copyright © 2015 purong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "HMSegmentedControl.h"
#import "IssueTableViewCell.h"
#import "IssueModel.h"

@interface IssueViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@end
