//
//  SubscriptionTableViewCell.m
//  RPMagazine
//
//  Created by purong on 23/9/15.
//  Copyright © 2015 purong. All rights reserved.
//

#import "SubscriptionTableViewCell.h"

@implementation SubscriptionTableViewCell

- (void)awakeFromNib {
    [_issueCoverImageView setContentMode:UIViewContentModeScaleAspectFill];
    [self setupReadButton];
    [self setupLabels];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)setupReadButton{
    [_issueReadBtn setBackgroundColor:[UIColor grayColor]];
    [_issueReadBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[_issueReadBtn layer]setBorderColor:[UIColor lightGrayColor].CGColor];
    [[_issueReadBtn layer]setBorderWidth:0.5];
    [[_issueReadBtn layer]setCornerRadius:5.0];
}

-(void)setupLabels{
    [_issueNumberLabel setNumberOfLines:0];
    [_issueNumberLabel setLineBreakMode:NSLineBreakByWordWrapping];
    
    [_issueDescriptionLabel setNumberOfLines:0];
    [_issueDescriptionLabel setLineBreakMode:NSLineBreakByWordWrapping];
}


@end
