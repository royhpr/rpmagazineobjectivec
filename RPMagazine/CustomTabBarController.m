//
//  CustomTabBarController.m
//  miceapps
//
//  Created by royhpr on 29/10/14.
//  Copyright (c) 2014 royhpr. All rights reserved.
//

#import "CustomTabBarController.h"

@interface CustomTabBarController()

@end

@implementation CustomTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
}

//overriding shouldRotate method for working in navController
-(BOOL)shouldAutorotate{
    return [self.selectedViewController shouldAutorotate];
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return [self.selectedViewController supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return [self.selectedViewController preferredInterfaceOrientationForPresentation];
}

@end
