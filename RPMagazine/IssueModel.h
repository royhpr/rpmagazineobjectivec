//
//  IssueModel.h
//  RPMagazine
//
//  Created by purong on 18/9/15.
//  Copyright © 2015 purong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IssueModel : NSObject

@property(nonatomic, strong)NSString *issueTitle;
@property(nonatomic, strong)NSString *issueDate;
@property(nonatomic, strong)NSString *issueDesc;
@property(nonatomic, strong)NSString *issuePrice;
@property(nonatomic, strong)NSString *issueCoverImageURL;
@property(nonatomic, strong)NSString *issueDownloadURL;

@end
