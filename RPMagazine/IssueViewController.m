//
//  IssueViewController.m
//  RPMagazine
//
//  Created by purong on 15/9/15.
//  Copyright © 2015 purong. All rights reserved.
//

#import "IssueViewController.h"

@interface IssueViewController ()

@property(nonatomic, strong)HMSegmentedControl *customSegmentControl;
@property(nonatomic, strong)UITableView *issueMainTable;
@property(nonatomic, strong)UISearchBar *issueSearchBar;

@property(nonatomic, strong)NSMutableArray *issueList;
@property(nonatomic, strong)NSMutableArray *articalList;
@property(nonatomic, strong)NSMutableArray *otherPublicationList;
@property(nonatomic, strong)NSMutableArray *searchList;

@property(nonatomic)NSInteger selectedSegmentIndex;

@end

@implementation IssueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Setup nav bar
    [self setupNavBar];
    
    //Setup list
    [self setupIssueList];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    //Draw custom segment
    [self setupCustomSegmentControl];
    
    //Draw table view
    [self setupIssueTableView];
}

-(void)setupNavBar{
    //add title image
    [self addNavTitleView];
    
    //Nav bar background color and tint color
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:BAR_COLOR_R/CUSTOM_COLOR_BASE green:BAR_COLOR_G/CUSTOM_COLOR_BASE blue:BAR_COLOR_B/CUSTOM_COLOR_BASE alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    //Add search button
    [self addSearchViewToNav];
}

-(void)addNavTitleView{
    UIImage *image = [UIImage imageNamed:IMG_RP_TITLE];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
}

-(void)addSearchViewToNav{
    UIImageView *searchImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, NAV_BAR_ICON_SIZE, NAV_BAR_ICON_SIZE)];
    [searchImageView setImage:[UIImage imageNamed:IMG_SEARCH]];
    UITapGestureRecognizer *searchSingleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapToSearchIssues)];
    [searchSingleTap setNumberOfTapsRequired:1];
    [searchSingleTap setNumberOfTouchesRequired:1];
    [searchImageView setUserInteractionEnabled:YES];
    [searchImageView addGestureRecognizer:searchSingleTap];
    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc]initWithCustomView:searchImageView];
    self.navigationItem.rightBarButtonItem = searchButton;
}

-(void)setupIssueList{
    _selectedSegmentIndex = 0;
    _issueList = [[NSMutableArray alloc]init];
    _articalList = [[NSMutableArray alloc]init];
    _otherPublicationList = [[NSMutableArray alloc]init];
    _searchList = [[NSMutableArray alloc]init];
    
    //TODO: remove it, for testing purpose, real time data will be fetched from server
    IssueModel *testIssue = [[IssueModel alloc]init];
    [testIssue setIssueTitle:@"ISSUE 01"];
    [testIssue setIssuePrice:@"Free"];
    [testIssue setIssueDesc:@"This is a test issue, blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah"];
    [testIssue setIssueDate:@"June 2015"];
    [testIssue setIssueCoverImageURL:@"NewsstandIcon@2x.png"];
    
    [_issueList addObject:testIssue];
}

-(void)setupCustomSegmentControl{
    if(_customSegmentControl){
        return;
    }
    
    CGFloat segmentOriginY = 0.0;
    CGFloat segmentHeight = 40.0;
    CGFloat segmentWidth = self.view.frame.size.width;
    
    _customSegmentControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"Wine Advocate", @"Articles", @"Other publications"]];
    _customSegmentControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    _customSegmentControl.frame = CGRectMake(0, segmentOriginY, segmentWidth, segmentHeight);
    _customSegmentControl.segmentEdgeInset = UIEdgeInsetsMake(0, 10, 0, 10);
    _customSegmentControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    _customSegmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    _customSegmentControl.verticalDividerEnabled = YES;
    _customSegmentControl.verticalDividerColor = [UIColor blackColor];
    _customSegmentControl.verticalDividerWidth = 1.0f;
    _customSegmentControl.backgroundColor = [UIColor colorWithRed:BAR_COLOR_R/CUSTOM_COLOR_BASE green:BAR_COLOR_G/CUSTOM_COLOR_BASE blue:BAR_COLOR_B/CUSTOM_COLOR_BASE alpha:1.0];
    _customSegmentControl.selectionIndicatorColor = [UIColor whiteColor];
    [_customSegmentControl setTitleFormatter:^NSAttributedString *(HMSegmentedControl *segmentedControl, NSString *title, NSUInteger index, BOOL selected) {
        NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
        return attString;
    }];
    [_customSegmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:_customSegmentControl];
    
    //Add autolayout constraint
    //[self setupAutolayoutConstraintToSegmentControl];
}

-(void)setupIssueTableView{
    if(_issueMainTable){
        return;
    }
    
    _issueMainTable = [[UITableView alloc] initWithFrame:CGRectMake(_customSegmentControl.frame.origin.x, _customSegmentControl.frame.size.height, _customSegmentControl.frame.size.width, self.view.frame.size.height - _customSegmentControl.frame.size.height) style:UITableViewStylePlain];
    _issueMainTable.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    _issueMainTable.delegate = self;
    _issueMainTable.dataSource = self;
    
    [self.view addSubview:_issueMainTable];
    
    //Add constraint
    //[self setupAutolayoutConstraintToIssueTableView];
    
    [_issueMainTable reloadData];
}

-(void)tapToSearchIssues{
    //Remove search view
    self.navigationItem.rightBarButtonItem = nil;
    
    //Add search bar instead
    _issueSearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0.0, 0.0, self.navigationController.navigationBar.frame.size.width, NAV_BAR_ICON_SIZE)];
    [_issueSearchBar setPlaceholder:@"Type to search issues..."];
    [_issueSearchBar setTintColor:[UIColor colorWithRed:168.0/CUSTOM_COLOR_BASE green:115.0/CUSTOM_COLOR_BASE blue:104.0/CUSTOM_COLOR_BASE alpha:1.0]];
    _issueSearchBar.delegate = self;
    
    [self.navigationItem setTitleView:_issueSearchBar];
    [_issueSearchBar becomeFirstResponder];
}

-(void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl{
    [_issueSearchBar resignFirstResponder];
    _selectedSegmentIndex = segmentedControl.selectedSegmentIndex;
    
    [_issueSearchBar removeFromSuperview];
    _issueSearchBar = nil;
    [self addNavTitleView];
    [self addSearchViewToNav];

    //Refresh the table data
    [_issueMainTable reloadData];
}

#pragma mark - Search bar delegate methods
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    searchBar.showsCancelButton = YES;
    
    if(_selectedSegmentIndex == 0)
    {
        [_searchList addObjectsFromArray:_issueList];
        [_issueList removeAllObjects];
    }
    else if(_selectedSegmentIndex == 1){
        [_searchList addObjectsFromArray:_articalList];
        [_articalList removeAllObjects];
    }
    else{
        [_searchList addObjectsFromArray:_otherPublicationList];
        [_otherPublicationList removeAllObjects];
    }
    
    [_issueMainTable reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self filterContentForSearchText:searchText];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    //Called when resign first respondant
    searchBar.showsCancelButton = NO;
    searchBar.text = @"";
    
    //Put back previous search list if it is not empty
    if(_searchList.count != 0){
        if(_selectedSegmentIndex == 0){
            [_issueList removeAllObjects];
            [_issueList addObjectsFromArray:_searchList];
        }
        else if(_selectedSegmentIndex == 1){
            [_articalList removeAllObjects];
            [_articalList addObjectsFromArray:_searchList];
        }
        else{
            [_otherPublicationList removeAllObjects];
            [_otherPublicationList addObjectsFromArray:_searchList];
        }
    }
    
    //Clear search list
    [_searchList removeAllObjects];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    
    //Remove search bar
    [_issueSearchBar removeFromSuperview];
    _issueSearchBar = nil;
    [self addNavTitleView];
    [self addSearchViewToNav];
    
    //Reload the isuue list
    [_issueMainTable reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    //Not in use
}

- (void)filterContentForSearchText:(NSString*)searchText{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF.issueTitle contains[cd] %@ OR SELF.issueDate contains[cd] %@ OR SELF.issueDesc contains[cd] %@ OR SELF.issuePrice contains[cd] %@",
                                    searchText, searchText, searchText, searchText];
    
    if(_selectedSegmentIndex == 0){
        [_issueList removeAllObjects];
        [_issueList addObjectsFromArray: [_searchList filteredArrayUsingPredicate:resultPredicate]];
    }
    else if(_selectedSegmentIndex == 1){
        [_articalList removeAllObjects];
        [_articalList addObjectsFromArray: [_searchList filteredArrayUsingPredicate:resultPredicate]];
    }
    else{
        [_otherPublicationList removeAllObjects];
        [_otherPublicationList addObjectsFromArray: [_searchList filteredArrayUsingPredicate:resultPredicate]];
    }
    
    [_issueMainTable reloadData];
}


#pragma mark - table view delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(_selectedSegmentIndex == 0){
        return _issueList.count;
    }
    else if(_selectedSegmentIndex == 1){
        return _articalList.count;
    }
    else{
        return _otherPublicationList.count;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"IssueCell";
    
    IssueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"IssueTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    IssueModel *currentIssue = (IssueModel*)[_issueList objectAtIndex:[indexPath row]];
    cell.issueNumberLabel.text = currentIssue.issueTitle;
    cell.issueDateLabel.text = currentIssue.issueDate;
    cell.issueDescriptionLabel.text = currentIssue.issueDesc;
    [cell.issuePriceBtn setTitle:currentIssue.issuePrice forState:UIControlStateNormal];
    [cell.issuePriceBtn setTitle:currentIssue.issuePrice forState:UIControlStateSelected];
    [cell.issueCoverImageView setImage:[UIImage imageNamed:currentIssue.issueCoverImageURL]]; //This will require asyn load of image URL
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //Do any thing necessary after this issue is tapped
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 180.0;
}

#pragma mark - for interface rotation
- (BOOL)shouldAutorotate{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma constraint code (used to support autolayout)
//-(void)setupAutolayoutConstraintToSegmentControl{
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_customSegmentControl
//                                                          attribute:NSLayoutAttributeTop
//                                                          relatedBy:NSLayoutRelationEqual
//                                                             toItem:self.topLayoutGuide
//                                                          attribute:NSLayoutAttributeBottomMargin
//                                                         multiplier:1
//                                                           constant:0.0]];
//    
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_customSegmentControl
//                                                          attribute:NSLayoutAttributeLeading
//                                                          relatedBy:NSLayoutRelationEqual
//                                                             toItem:self.view
//                                                          attribute:NSLayoutAttributeTrailingMargin
//                                                         multiplier:1.0
//                                                           constant:0.0]];
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_customSegmentControl
//                                                          attribute:NSLayoutAttributeTrailing
//                                                          relatedBy:NSLayoutRelationEqual
//                                                             toItem:self.view
//                                                          attribute:NSLayoutAttributeLeadingMargin
//                                                         multiplier:1.0
//                                                           constant:0.0]];
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_customSegmentControl
//                                                          attribute:NSLayoutAttributeHeight
//                                                          relatedBy:NSLayoutRelationEqual
//                                                             toItem:nil
//                                                          attribute:NSLayoutAttributeHeight
//                                                         multiplier:1.0
//                                                           constant:40.0]];
//    
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_customSegmentControl
//                                                          attribute:NSLayoutAttributeTop
//                                                          relatedBy:NSLayoutRelationEqual
//                                                             toItem:self.topLayoutGuide
//                                                          attribute:NSLayoutAttributeBottomMargin
//                                                         multiplier:1
//                                                           constant:0.0]];
//}

//-(void)setupAutolayoutConstraintToIssueTableView{
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_issueMainTable
//                                                          attribute:NSLayoutAttributeTop
//                                                          relatedBy:NSLayoutRelationEqual
//                                                             toItem:_customSegmentControl
//                                                          attribute:NSLayoutAttributeBottomMargin
//                                                         multiplier:1.0
//                                                           constant:0]];
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_issueMainTable
//                                                          attribute:NSLayoutAttributeLeading
//                                                          relatedBy:NSLayoutRelationEqual
//                                                             toItem:self.view
//                                                          attribute:NSLayoutAttributeTrailingMargin
//                                                         multiplier:1.0
//                                                           constant:0.0]];
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_issueMainTable
//                                                          attribute:NSLayoutAttributeTrailing
//                                                          relatedBy:NSLayoutRelationEqual
//                                                             toItem:self.view
//                                                          attribute:NSLayoutAttributeLeadingMargin
//                                                         multiplier:1.0
//                                                           constant:0.0]];
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_issueMainTable
//                                                          attribute:NSLayoutAttributeBottom
//                                                          relatedBy:NSLayoutRelationEqual
//                                                             toItem:self.bottomLayoutGuide
//                                                          attribute:NSLayoutAttributeTopMargin
//                                                         multiplier:1.0
//                                                           constant:0]];
//}
@end
