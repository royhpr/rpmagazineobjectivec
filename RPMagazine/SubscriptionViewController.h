//
//  SubscriptionViewController.h
//  RPMagazine
//
//  Created by purong on 23/9/15.
//  Copyright © 2015 purong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "SubscriptionTableViewCell.h"
#import "IssueModel.h"

@interface SubscriptionViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UITableView *subscriptionMainTable;

@end
