//
//  main.m
//  RPMagazine
//
//  Created by purong on 15/9/15.
//  Copyright © 2015 purong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
